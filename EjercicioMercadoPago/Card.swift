//
//  Card.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Card: PaymentMethod {
    
    init(json: JSON) {
        let settingsJson = json["settings"][0]
        let cardNumberSetting = CardNumber(json: settingsJson["card_number"])
        let binSetting = CardBin(json: settingsJson["bin"])
        let securityCodeSetting = CardSecurityCode(json: settingsJson["security_code"])
        
        let cardSetting = CardSetting(number: cardNumberSetting, bin: binSetting, securityCode: securityCodeSetting)
        let settings: [PaymentMethodSetting] = [cardSetting]
        
        super.init(id: json["id"].stringValue, name: json["name"].stringValue, status: json["status"].stringValue, thumbnailName: json["thumbnail"].stringValue, settings: settings)
    }
}
