//
//  InstallmentsModel.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

class InstallmentsModel {
    
    fileprivate var installments = [Installment]()
    let amount: Float
    let paymentMethodId: String
    let issuerId: String
    
    init(amount: Float, paymentMethodId: String, issuerId: String) {
        self.amount = amount
        self.paymentMethodId = paymentMethodId
        self.issuerId = issuerId
    }
}

extension InstallmentsModel: MPTableViewDataSource {
    
    var count: Int {
        return installments.count
    }
    
    func objectAtIndex(_ index: Int) -> MPTableRowObject {
        return installments[index]
    }
    
    func fetchData(_ completion: @escaping FetchDataCompletion, _ error: @escaping FetchDataCompletionError) {
        MPDatasource.getInstallments(forAmount: amount,
                                     paymentMethodId: paymentMethodId,
                                     issuerId: issuerId,
                                     completion: { [weak self] (installments) in
                                        self?.installments = installments
                                        completion()
            },
                                     errorHandler: {
                                        error()
        })
    }
}
