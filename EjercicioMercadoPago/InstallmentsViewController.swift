//
//  InstallmentsViewController.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class InstallmentsViewController: BaseTableViewController {
    
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var paymentMethodImageView: UIImageView!
    @IBOutlet var paymentMethodNameLabel: UILabel!
    @IBOutlet var issuerImageView: UIImageView!
    @IBOutlet var issuerNameLabel: UILabel!
    
    var delegate: MPAppCoordinatorProtocol?
    
    let amount: Float
    let paymentMethod: PaymentMethod
    let issuer: Bank
    
    convenience init(amount: Float, paymentMethod: PaymentMethod, issuer: Bank) {
        let dataSource = InstallmentsModel(amount: amount, paymentMethodId: paymentMethod.id, issuerId: issuer.id)
        let viewDelegate = InstallmentsViewViewModel()
        self.init(amount: amount, paymentMethod: paymentMethod, issuer: issuer, dataSource: dataSource, viewDelegate: viewDelegate)
        
        //Para evitar ser delegado del viewDelegate, podria utilizarse ReactiveCocoa y suscribirse a sus señales
        viewDelegate.delegate = self
    }
    
    required init(amount: Float, paymentMethod: PaymentMethod, issuer: Bank, dataSource: MPTableViewDataSource, viewDelegate: MPTableViewDelegate) {
        self.amount = amount
        self.paymentMethod = paymentMethod
        self.issuer = issuer
        super.init(dataSource: dataSource, viewDelegate: viewDelegate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        self.amountLabel.text = "Monto a abonar: \(amount.currencyString())"
        self.paymentMethodNameLabel.text = self.paymentMethod.name
        self.paymentMethodImageView.loadRemoteImage(url: paymentMethod.thumbnailName)
        self.issuerNameLabel.text = issuer.name
        self.issuerImageView.loadRemoteImage(url: issuer.thumbnail)
        
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationItem.title = "Cuotas"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(popViewController))
    }
}

extension InstallmentsViewController: MPViewViewModelProtocol {
    
    func objectSelected(_ object: MPTableRowObject) {
        guard let installment = object as? Installment else {
            return
        }
        self.delegate?.didSelectInstallment(installment: installment)
    }
    
}
