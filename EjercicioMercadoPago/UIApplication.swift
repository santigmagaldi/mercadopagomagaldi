//
//  UIApplication.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/15/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
