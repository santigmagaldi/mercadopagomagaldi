//
//  InstallmentsView.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class InstallmentsViewViewModel {
    fileprivate let rowHeight: CGFloat = 45.0
    
    var delegate: MPViewViewModelProtocol?
}

extension InstallmentsViewViewModel: MPTableViewDelegate {

    var supportedType: MPTableViewRowType {
        return .installmentsSelection
    }
    
    func cellHeightForRowType(_ type: MPTableViewRowType) -> CGFloat {
        return rowHeight
    }
    
    func cellClassForRowType(_ type: MPTableViewRowType) -> UITableViewCell.Type {
        return InstallmentTableViewCell.self
    }
    
    func reusableCellIdentifierForRowType(_ type: MPTableViewRowType) -> String {
        return type.rawValue
    }
    
    func objectForRowType(_ type: MPTableViewRowType) -> MPTableRowObject.Type {
        return Installment.self
    }
    
    func didSelectObject(_ object: MPTableRowObject) {
        delegate?.objectSelected(object)
    }
    
}
