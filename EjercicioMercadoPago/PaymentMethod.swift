//
//  PaymentMethod.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation
import SwiftyJSON

class PaymentMethod {
    let id: String
    let name: String
    let status: String
    let thumbnailName: String
    var settings: [PaymentMethodSetting]
    
    init(id: String, name: String, status: String, thumbnailName: String, settings: [PaymentMethodSetting]) {
        self.id = id
        self.name = name
        self.status = status
        self.thumbnailName = thumbnailName
        self.settings = settings
    }
}

extension PaymentMethod: MPTableRowObject {
    var type: MPTableViewRowType {
        return .paymentMethod
    }
}
