//
//  PaymentMethodModel.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

class PaymentMethodModel {
    
    fileprivate let amount: Float
    fileprivate var paymentMethods = [PaymentMethod]()
    
    init(amount: Float) {
        self.amount = amount
    }
}

extension PaymentMethodModel: MPTableViewDataSource {
    
    var count: Int {
        return paymentMethods.count
    }
    
    func objectAtIndex(_ index: Int) -> MPTableRowObject {
        return paymentMethods[index]
    }
    
    func fetchData(_ completion: @escaping FetchDataCompletion, _ error: @escaping FetchDataCompletionError) {
        
        MPDatasource.getPaymentMethods(completion: { [weak self] (methods) in
            self?.paymentMethods = methods
            completion()
        }, errorHandler: {
            error()
        })
    }
}
