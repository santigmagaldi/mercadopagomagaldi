//
//  BankViewViewModel.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class BankViewViewModel {
    
    let rowHeight: CGFloat = 55.0
    
    var delegate: MPViewViewModelProtocol?
    
}

extension BankViewViewModel: MPTableViewDelegate {
    
    var supportedType: MPTableViewRowType {
        return .bankSelection
    }
    
    func cellHeightForRowType(_ type: MPTableViewRowType) -> CGFloat {
        return rowHeight
    }
    
    func cellClassForRowType(_ type: MPTableViewRowType) -> UITableViewCell.Type {
        return BankTableViewCell.self
    }
    
    func reusableCellIdentifierForRowType(_ type: MPTableViewRowType) -> String {
        return type.rawValue
    }
    
    func objectForRowType(_ type: MPTableViewRowType) -> MPTableRowObject.Type {
        return Bank.self
    }
    
    func didSelectObject(_ object: MPTableRowObject) {
        delegate?.objectSelected(object)
    }
    
}
