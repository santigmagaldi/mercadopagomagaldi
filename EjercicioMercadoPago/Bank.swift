//
//  Bank.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit
import SwiftyJSON

class Bank: NSObject {
    
    var id: String
    var name: String
    var secureThumbnail: String
    var thumbnail: String
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.secureThumbnail = json["secure_thumbnail"].stringValue
        self.thumbnail = json["thumbnail"].stringValue
    }
}

extension Bank: MPTableRowObject {
    var type: MPTableViewRowType {
        return .bankSelection
    }
}
