//
//  PFDatasource.swift
//  PartyFans
//
//  Created by Santiago Gutierrrez on 3/4/17.
//  Copyright © 2017 Ritvu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MPDatasource {
    
    public typealias PaymentMethodsCompletionHandler = (_ paymentMethods: [PaymentMethod]) -> Void
    public typealias BanksCompletionHandler = (_ banks: [Bank]) -> Void
    public typealias InstallmentsCompletionHandler = (_ installments: [Installment]) -> Void
    public typealias ErroHandler = () -> Void
    
    class func getPaymentMethods(completion:@escaping PaymentMethodsCompletionHandler,
                                 errorHandler:@escaping ErroHandler) {

        let apiPublicKey = MercadoPagoKeys.apiPublicKey()
        let params = ["public_key": apiPublicKey]
        APIRequest.getPaymentMethods(params, completition: { (response) in
            
        if let res = response,
            let paymentMethods = JSON(res).array {
            var methods:[Card] = []
            for method in paymentMethods.filter({ $0["payment_type_id"] == "credit_card"}) {
                let paymentMethod = Card(json: method)
                methods.append(paymentMethod)
            }
            completion(methods)
            return
        }
            errorHandler()
        }, error: {
            errorHandler()
        })
    }
    
    class func getBanks(forId paymentMethodId: String,
                        completion:@escaping BanksCompletionHandler,
                        errorHandler:@escaping ErroHandler) {
        
        let apiPublicKey = MercadoPagoKeys.apiPublicKey()
        let params = ["public_key": apiPublicKey, "payment_method_id": paymentMethodId]
        APIRequest.getBanks(params, completition: { (response) in
            
            if let res = response,
                let banksJson = JSON(res).array {
                var banks:[Bank] = []
                for bankJson in banksJson {
                    let bank = Bank(json: bankJson)
                    banks.append(bank)
                }
                completion(banks)
                return
            }
            errorHandler()
        }, error: {
            errorHandler()
        })
    }
    
    class func getInstallments(forAmount amount: Float,
                        paymentMethodId: String,
                        issuerId: String,
                        completion:@escaping InstallmentsCompletionHandler,
                        errorHandler:@escaping ErroHandler) {
        
        let apiPublicKey = MercadoPagoKeys.apiPublicKey()
        let params = ["public_key": apiPublicKey,
                      "amount": amount,
                      "payment_method_id": paymentMethodId,
                      "issuer_id": issuerId] as [String : Any]
        APIRequest.getInstallments(params, completition: { (response) in
            
            if let res = response,
                let resposeJson = JSON(res)[0]["payer_costs"].array {
                var installments:[Installment] = []
                for payerInstallment in resposeJson {
                    let installment = Installment(json: payerInstallment)
                    installments.append(installment)
                }
                completion(installments)
                return
            }
            errorHandler()
        }, error: {
            errorHandler()
        })
    }
}
