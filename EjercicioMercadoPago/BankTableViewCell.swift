//
//  BankTableViewCell.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class BankTableViewCell: UITableViewCell {
    
    @IBOutlet var bankImageView: UIImageView!
    @IBOutlet var bankNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

extension BankTableViewCell: MPTableViewCellProtocol {
    func setupCellWithObject(_ object: MPTableRowObject) {
        guard let bank = object as? Bank else {
            return
        }
        self.bankNameLabel.text = bank.name
        self.bankImageView.loadRemoteImage(url: bank.thumbnail)
    }
}
