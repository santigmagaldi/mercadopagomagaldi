//
//  Float.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/15/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

extension Float {
    
    func currencyString() -> String {
        return String(format: "$%.02f", self)
    }
    
}
