//
//  UIImageView.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

extension UIImageView {

    func loadRemoteImage(url: String) {
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                self.image = image
            }
        }
    }

}
