//
//  PaymentMethodTableViewCell.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet var paymentNameLabel: UILabel!
    @IBOutlet var paymentMethodImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension PaymentMethodTableViewCell: MPTableViewCellProtocol {
    
    func setupCellWithObject(_ object: MPTableRowObject) {
        guard let method = object as? PaymentMethod else {
            return
        }
        self.paymentNameLabel.text = method.name
        self.paymentMethodImageView.loadRemoteImage(url: method.thumbnailName)
    }
}
