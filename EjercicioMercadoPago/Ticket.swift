//
//  Ticket.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

class Ticket: PaymentMethod {
    
    let financialInstitutions:[String]
    
    init(id: String, name: String, status: String, thumbnailName: String, financialInstitutions: [String], settings:[PaymentMethodSetting]) {
        self.financialInstitutions = financialInstitutions
        super.init(id: id, name: name, status: status, thumbnailName: thumbnailName, settings: settings)
    }
}
