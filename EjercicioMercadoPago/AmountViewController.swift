//
//  AmountViewController.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class AmountViewController: UIViewController {
    
    var delegate: MPAppCoordinatorProtocol?

    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var continueButton: UIButton!
    @IBOutlet var viewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deregisterKeyboardListeners()
    }

    private func setupViews() {
        setupNavigationBar()
        amountTextField.becomeFirstResponder()
        continueButton.backgroundColor = UIColor().primaryColor()
        registerKeyboardListeners()
    }
    
    private func setupNavigationBar() {
        navigationItem.title = "Nuevo pago"
    }
    
    @IBAction func didPressContinueButton(_ sender: Any) {
        guard let value = amountTextField.text,
            let amount = Float(value) else {
                showAmountError()
            return
        }
        amountTextField.text = ""
        delegate?.didEnterAmount(amount: amount)
    }
    
    private func showAmountError() {
        let errorAlert = UIAlertController(title: "Error", message: "Por favor, verifique el monto ingresado", preferredStyle: .alert)
        let confirmationAction = UIAlertAction(title: "Aceptar",
                                               style: .default, handler: nil)
        errorAlert.addAction(confirmationAction)
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    //MARK keyboard listeners
    
    func registerKeyboardListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func deregisterKeyboardListeners(){
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: Notification) {
        //Cuando aparece el teclado, modifico la constraint del botón de continuar para que se situe arriba del teclado
        let userInfo = notification.userInfo
        guard let keyboardSize = userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        viewBottomConstraint.constant = keyboardSize.cgRectValue.height
    }
    
}
