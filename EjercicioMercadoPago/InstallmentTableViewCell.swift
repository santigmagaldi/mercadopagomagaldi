//
//  InstallmentTableViewCell.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class InstallmentTableViewCell: UITableViewCell {
    
    @IBOutlet var installmentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension InstallmentTableViewCell: MPTableViewCellProtocol {
    func setupCellWithObject(_ object: MPTableRowObject) {
        guard let installment = object as? Installment else {
            return
        }
        self.installmentLabel.text = installment.recommendedMessage
    }
}
