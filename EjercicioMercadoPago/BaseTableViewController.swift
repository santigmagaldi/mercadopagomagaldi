//
//  BaseTableViewController.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit
import SnapKit

typealias FetchDataCompletion = () -> Void
typealias FetchDataCompletionError = () -> Void

protocol MPTableRowObject {
    var type: MPTableViewRowType { get }
}

enum MPTableViewRowType: String {
    case paymentMethod = "PaymentMethod"
    case bankSelection = "BankSelection"
    case installmentsSelection = "InstallmentsSelection"
}

protocol MPTableViewDataSource {
    var count: Int { get }
    
    func objectAtIndex(_ index: Int) -> MPTableRowObject
    func fetchData(_ completion: @escaping FetchDataCompletion, _ error: @escaping FetchDataCompletionError)
}

protocol MPTableViewDelegate {
    var supportedType: MPTableViewRowType { get }
    
    func objectForRowType(_ type: MPTableViewRowType) -> MPTableRowObject.Type
    func reusableCellIdentifierForRowType(_ type: MPTableViewRowType) -> String
    func cellClassForRowType(_ type: MPTableViewRowType) -> UITableViewCell.Type
    func cellHeightForRowType (_ type: MPTableViewRowType) -> CGFloat
    func didSelectObject(_ object: MPTableRowObject)
}

protocol MPTableViewCellProtocol {
    func setupCellWithObject(_ object: MPTableRowObject)
}

protocol MPViewViewModelProtocol {
    func objectSelected(_ object: MPTableRowObject)
}

class BaseTableViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var activityIndicatorView: UIActivityIndicatorView?
    let noResultsLabel = UILabel()
    
    var dataSource: MPTableViewDataSource
    var viewDelegate: MPTableViewDelegate
    
    init(dataSource: MPTableViewDataSource, viewDelegate: MPTableViewDelegate) {
        self.dataSource = dataSource
        self.viewDelegate = viewDelegate
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNoResultsViews()
        setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityIndicatorView = showLoadingActivityIndicatorView()
        self.tableView.isHidden = true
        dataSource.fetchData({ [weak self] in
            self?.handleDataFetchResult()
        }) { [weak self] in
            self?.showRequestError()
        }
    }
    
    private func handleDataFetchResult() {
        self.hideIndicatorView()
        if dataSource.count > 0 {
            self.tableView.isHidden = false
            self.tableView.reloadData()
        } else {
            noResultsLabel.isHidden = false
        }
    }
    
    private func setupNoResultsViews() {
        noResultsLabel.text = "No se han encontrado resultados"
        noResultsLabel.font = noResultsLabel.font.withSize(14)
        self.view.addSubview(noResultsLabel)
        
        noResultsLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.view).offset(20)
            make.trailing.equalTo(self.view).offset(20)
            make.top.equalTo(self.tableView)
        }
        
        noResultsLabel.isHidden = true
    }
    
    private func setupTableView() {
        tableView.isHidden = true
        tableView.tableFooterView = UIView()
        
        let viewType = viewDelegate.supportedType
        let cellIdentifier = viewDelegate.reusableCellIdentifierForRowType(viewType)
        let cellClass: AnyClass = viewDelegate.cellClassForRowType(viewType)
        
        let cellNib = UINib(nibName: String(describing: cellClass), bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
    }
    
    private func hideIndicatorView() {
        if let indicatorView = self.activityIndicatorView {
            self.hideLoading(activityIndicatorView: indicatorView)
        }
    }

}

extension BaseTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowObject = dataSource.objectAtIndex(indexPath.row)
        let cellIdentifier = viewDelegate.reusableCellIdentifierForRowType(rowObject.type)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        if let setupableCell = cell as? MPTableViewCellProtocol {
            setupableCell.setupCellWithObject(rowObject)
        }
        
        return cell
    }
    
}

extension BaseTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowObject = dataSource.objectAtIndex(indexPath.row)
        return viewDelegate.cellHeightForRowType(rowObject.type)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let rowObject = dataSource.objectAtIndex(indexPath.row)
        viewDelegate.didSelectObject(rowObject)
    }
}
