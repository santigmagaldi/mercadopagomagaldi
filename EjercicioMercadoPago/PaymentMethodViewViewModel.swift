//
//  PaymentMethodViewViewModel.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class PaymentMethodViewViewModel {
    
    let rowHeight: CGFloat = 55.0
    
    var delegate: MPViewViewModelProtocol?
}

extension PaymentMethodViewViewModel: MPTableViewDelegate {
    
    var supportedType: MPTableViewRowType {
        return .paymentMethod
    }
    
    func cellHeightForRowType(_ type: MPTableViewRowType) -> CGFloat {
        return rowHeight
    }
    
    func cellClassForRowType(_ type: MPTableViewRowType) -> UITableViewCell.Type {
        return PaymentMethodTableViewCell.self
    }
    
    func reusableCellIdentifierForRowType(_ type: MPTableViewRowType) -> String {
        return type.rawValue
    }
    
    func objectForRowType(_ type: MPTableViewRowType) -> MPTableRowObject.Type {
        return PaymentMethod.self
    }
    
    func didSelectObject(_ object: MPTableRowObject) {
        delegate?.objectSelected(object)
    }
    
}
