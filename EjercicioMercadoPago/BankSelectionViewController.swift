//
//  BankSelectionViewController.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class BankSelectionViewController: BaseTableViewController {

    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var paymentMethodImageView: UIImageView!
    @IBOutlet var paymentMethodNameLabel: UILabel!
    
    var delegate: MPAppCoordinatorProtocol?
    
    let amount: Float
    var paymentMethod: PaymentMethod
    
    convenience init(amount: Float, paymentMethod: PaymentMethod) {
        let dataSource = BankSelectionModel(amount: amount, paymentMethodId: paymentMethod.id)
        let viewDelegate = BankViewViewModel()
        self.init(amount: amount, paymentMethod: paymentMethod, dataSource: dataSource, viewDelegate: viewDelegate)
        
        //Para evitar ser delegado del viewDelegate, podria utilizarse ReactiveCocoa y suscribirse a sus señales
        viewDelegate.delegate = self
    }
    
    required init(amount: Float, paymentMethod: PaymentMethod, dataSource: MPTableViewDataSource, viewDelegate: MPTableViewDelegate) {
        self.amount = amount
        self.paymentMethod = paymentMethod
        super.init(dataSource: dataSource, viewDelegate: viewDelegate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        self.amountLabel.text = "Monto a abonar: \(self.amount.currencyString())"
        self.paymentMethodNameLabel.text = self.paymentMethod.name
        self.paymentMethodImageView.loadRemoteImage(url: self.paymentMethod.thumbnailName)
    }
}

extension BankSelectionViewController: MPViewViewModelProtocol {
    
    func objectSelected(_ object: MPTableRowObject) {
        guard let bank = object as? Bank else {
            return
        }
        self.delegate?.didSelectIssuer(issuer: bank)
    }
    
}
