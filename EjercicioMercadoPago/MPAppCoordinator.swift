//
//  MPAppCoordinator.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class MPAppCoordinator: NSObject {
    
    let navigationController: UINavigationController
    
    var amount: Float?
    var paymentMethod: PaymentMethod?
    var issuer: Bank?
    var installment: Installment?
    
    override init() {
        let rootViewController = AmountViewController()
        navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.navigationBar.isTranslucent = false
        
        super.init()
        rootViewController.delegate = self
    }
    
    func start() -> UIViewController {
        return self.navigationController
    }
}

extension MPAppCoordinator: MPAppCoordinatorProtocol {
    
    func didEnterAmount(amount: Float) {
        self.amount = amount
        let paymentMethodPickerController = PaymentMethodPickerViewController(amount: amount)
        paymentMethodPickerController.delegate = self
        self.navigationController.pushViewController(paymentMethodPickerController, animated: true)
    }
    
    func didSelectPaymentMethod(method: PaymentMethod) {
        self.paymentMethod = method
        
        if let amount = self.amount,
            let paymentMethod = self.paymentMethod {
            let bankPickerController = BankSelectionViewController(amount: amount, paymentMethod: paymentMethod)
            bankPickerController.delegate = self
            self.navigationController.pushViewController(bankPickerController, animated: true)
        }
    }

    func didSelectIssuer(issuer: Bank) {
        self.issuer = issuer
        
        if let amount = self.amount,
            let paymentMethod = self.paymentMethod,
            let issuer = self.issuer {
            let installmentsViewController = InstallmentsViewController(amount: amount, paymentMethod: paymentMethod, issuer: issuer)
            installmentsViewController.delegate = self
            self.navigationController.pushViewController(installmentsViewController, animated: true)
        }
    }
    
    func didSelectInstallment(installment: Installment) {
        self.installment = installment
        self.navigationController.popToRootViewController(animated: true)
        self.showConfirmationAlert()
    }
    
    fileprivate func showConfirmationAlert() {
        guard let amount = amount,
            let paymentMethod = paymentMethod,
            let issuer = issuer,
            let installment = installment else {
                return
        
        }
        
        let alertTitle = "Confirmación"
        let alertMessage = "Desea confirmar el pago de $\(amount), abonando con \(paymentMethod.name) de \(issuer.name) en \(installment.recommendedMessage)?"
        
        let alert = UIAlertController(title: alertTitle,
                                      message: alertMessage,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let confirmationAction = UIAlertAction(title: "Confirmar",
                                         style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancelar",
                                         style: .cancel, handler: nil)
        
        alert.addAction(confirmationAction)
        alert.addAction(cancelAction)
        self.navigationController.present(alert, animated: true, completion: nil)
    }
    
}
