//
//  BankSelectionModel.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/16/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

class BankSelectionModel {
    
    var banks = [Bank]()
    
    let amount: Float
    let paymentMethodId: String
    
    init(amount: Float, paymentMethodId: String) {
        self.amount = amount
        self.paymentMethodId = paymentMethodId
    }
    
}

extension BankSelectionModel: MPTableViewDataSource {
    
    var count: Int {
        return banks.count
    }
    
    func objectAtIndex(_ index: Int) -> MPTableRowObject {
        return banks[index]
    }
    
    func fetchData(_ completion: @escaping FetchDataCompletion, _ error: @escaping FetchDataCompletionError) {
        MPDatasource.getBanks(forId: paymentMethodId, completion: { [weak self] (banks) in
            self?.banks = banks
            completion()
            }, errorHandler: {
                error()
        })
    }
}
