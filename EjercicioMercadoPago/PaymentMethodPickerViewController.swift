//
//  PaymentMethodPickerViewController.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit

class PaymentMethodPickerViewController: BaseTableViewController {

    @IBOutlet var amountLabel: UILabel!
    
    var delegate: MPAppCoordinatorProtocol?
    
    fileprivate let amount: Float
    
    convenience init(amount: Float) {
        let dataSource = PaymentMethodModel(amount: amount)
        let viewDelegate = PaymentMethodViewViewModel()
        self.init(amount: amount, dataSource: dataSource, viewDelegate: viewDelegate)
        
        //Para evitar ser delegado del viewDelegate, podria utilizarse ReactiveCocoa y suscribirse a sus señales
        viewDelegate.delegate = self
    }
    
    required init(amount: Float, dataSource: MPTableViewDataSource, viewDelegate: MPTableViewDelegate) {
        self.amount = amount
        super.init(dataSource: dataSource, viewDelegate: viewDelegate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        self.amountLabel.text = "Monto a abonar: \(self.amount.currencyString())"
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        navigationItem.title = "Método de pago"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(popViewController))
    }
}

extension PaymentMethodPickerViewController: MPViewViewModelProtocol {
    
    func objectSelected(_ object: MPTableRowObject) {
        guard let paymentMethod = object as? PaymentMethod else {
            return
        }
        self.delegate?.didSelectPaymentMethod(method: paymentMethod)
    }
}
