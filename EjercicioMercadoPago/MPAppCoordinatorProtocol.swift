//
//  MPAppCoordinatorProtocol.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/12/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

protocol MPAppCoordinatorProtocol {
    func didEnterAmount(amount: Float)
    func didSelectPaymentMethod(method: PaymentMethod)
    func didSelectIssuer(issuer: Bank)
    func didSelectInstallment(installment: Installment)
}
