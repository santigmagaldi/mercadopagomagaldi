//
//  APIRequest.swift
//  PartyFans
//
//  Created by Santiago Gutierrrez on 3/4/17.
//  Copyright © 2017 Ritvu. All rights reserved.
//

import Foundation
import Alamofire

struct Resources {
    static let urlPaymentMethods = "/payment_methods" // GET
    static let urlCardIssuers = "/payment_methods/card_issuers" // GET
    static let urlInstallments = "/payment_methods/installments" // GET
}

class APIRequest {
    
    class func commonGetRequest (_ params:Parameters?, url: String, completition:@escaping (_ responseObject:AnyObject) -> Void, error:@escaping () -> Void) {
        
        let apiBaseUrl = MercadoPagoKeys.apiBaseUrl()
        let apiVersion = MercadoPagoKeys.apiVersion()
        
        let completeUrl = apiBaseUrl + apiVersion + url
        
        Alamofire.request(completeUrl, method: .get, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                if response.result.isSuccess {
                    completition(response.result.value as AnyObject)
                } else {
                    error()
                }
        }
    }
    
    class func getPaymentMethods(_ params:Parameters, completition:@escaping (_ responseObject: AnyObject?) -> Void, error:@escaping () -> Void) {
        commonGetRequest(params, url: Resources.urlPaymentMethods, completition: { (response) in
            completition(response)
        }, error: {
            error()
        })
    }
    
    class func getBanks(_ params:Parameters, completition:@escaping (_ responseObject: AnyObject?) -> Void, error:@escaping () -> Void) {
        commonGetRequest(params, url: Resources.urlCardIssuers, completition: { (response) in
            completition(response)
        }, error: {
            error()
        })
    }
    
    class func getInstallments(_ params:Parameters, completition:@escaping (_ responseObject: AnyObject?) -> Void, error:@escaping () -> Void) {
        commonGetRequest(params, url: Resources.urlInstallments, completition: { (response) in
            completition(response)
        }, error: {
            error()
        })
    }

}
