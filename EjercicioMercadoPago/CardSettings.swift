//
//  CardNumber.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/18/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation
import SwiftyJSON

class CardSetting: PaymentMethodSetting {
    let number: CardNumber
    let bin: CardBin
    let securityCode: CardSecurityCode
    
    init(number: CardNumber, bin: CardBin, securityCode: CardSecurityCode) {
        self.number = number
        self.bin = bin
        self.securityCode = securityCode
        super.init()
    }
}

class CardNumber {
    let validation: String
    let length: Int
    
    init(json: JSON) {
        self.validation = json["validation"].stringValue
        self.length = json["length"].intValue
    }
}

class CardBin {
    let pattern: String
    let installmentsPattern: String
    let exclusionPattern: String?
    
    init(json: JSON) {
        self.pattern = json["pattern"].stringValue
        self.installmentsPattern = json["installments_pattern"].stringValue
        self.exclusionPattern = json["exclusion_pattern"].stringValue
    }
}

class CardSecurityCode {
    let length: Int
    let cardLocation: String
    let mode: String
    
    init(json: JSON) {
        self.length = json["length"].intValue
        self.cardLocation = json["cardLocation"].stringValue
        self.mode = json["mode"].stringValue
    }
}
