//
//  MercadoPagoKeys.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation

class MercadoPagoKeys {
    
    static let API_BASE_URL_KEY = "API_BASE_URL"
    static let API_VERSION_KEY = "API_VERSION"
    static let API_PUBLIC_KEY = "API_PUBLIC_KEY"
    
    class func apiBaseUrl() -> String {
        return self.key(forKeyId: API_BASE_URL_KEY)
    }
    
    class func apiVersion() -> String {
        return self.key(forKeyId: API_VERSION_KEY)
    }
    
    class func apiPublicKey() -> String {
        return self.key(forKeyId: API_PUBLIC_KEY)
    }
    
    class func key(forKeyId keyId: String) -> String {
        return Bundle.main.infoDictionary?[keyId] as! String
    }
}
