//
//  UIViewController.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/15/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import UIKit
import SnapKit

extension UIViewController {
    
    func popViewController() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showLoadingActivityIndicatorView() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(self.view)
        }
        activityIndicator.startAnimating()
        return activityIndicator
    }
    
    func hideLoading(activityIndicatorView : UIActivityIndicatorView) {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true
    }
    
    func showRequestError() {
        let errorAlert = UIAlertController(title: "Error", message: "Ha ocurrido un error. Intente nuevamente", preferredStyle: .alert)
        let confirmationAction = UIAlertAction(title: "Aceptar",
                                               style: .default, handler: nil)
        errorAlert.addAction(confirmationAction)
        self.present(errorAlert, animated: true, completion: nil)
    }
    
}
