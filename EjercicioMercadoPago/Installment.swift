//
//  Installment.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/13/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Installment {
    
    var installments: Int
    var installmentRate: Float
    var discountRate: Float
    var recommendedMessage: String
    
    init(json: JSON) {
        self.installments = json["installments"].intValue
        self.installmentRate = json["installment_rate"].floatValue
        self.discountRate = json["discount_rate"].floatValue
        self.recommendedMessage = json["recommended_message"].stringValue
    }
}

extension Installment: MPTableRowObject {
    var type: MPTableViewRowType {
        return .installmentsSelection
    }
}
