//
//  MPPaymentMethodsTests.swift
//  EjercicioMercadoPago
//
//  Created by Santiago Gutierrrez on 5/17/17.
//  Copyright © 2017 Santiago Magaldi. All rights reserved.
//

import XCTest
import Alamofire
import SwiftyJSON

class MPPaymentMethodsTests: XCTestCase {
    
    var sessionUnderTest: URLSession!
    var expectation:XCTestExpectation?
    var paymentMethodsUrlString: String!
    var testPaymentMethodId = "visa"
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
        let baseUrl = MercadoPagoKeys.apiBaseUrl() + MercadoPagoKeys.apiVersion() + "/payment_methods"
        let apiPublicKey = MercadoPagoKeys.apiPublicKey()
        paymentMethodsUrlString = baseUrl + "?public_key=" + apiPublicKey
    }
    
    override func tearDown() {
        sessionUnderTest = nil
        expectation = nil
        super.tearDown()
    }
    
    //MARK Payment Methods
    
    func testPaymentMethodsResponse() {
        expectation = expectation(description: paymentMethodsUrlString)
        
        let apiPublicKey = MercadoPagoKeys.apiPublicKey()
        let params = ["public_key": apiPublicKey]
        
        APIRequest.getPaymentMethods(params, completition: { (response) in
            
            XCTAssertNotNil(response)
            
            if let res = response,
                let paymentMethods = JSON(res).array {
                for method in paymentMethods.filter({ $0["payment_type_id"] == "credit_card"}) {
                    XCTAssertTrue(method["id"].exists(), "No devuelve id")
                    XCTAssertTrue(method["name"].exists(), "No devuelve name")
                    XCTAssertTrue(method["status"].exists(), "No devuelve status")
                    XCTAssertTrue(method["thumbnail"].exists(), "No devuelve thumbnail")
                }
            }
        }, error: {
            XCTAssert(false)
        })
        
        self.expectation?.fulfill()
        
        waitForExpectations(timeout: 5) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }    
}
